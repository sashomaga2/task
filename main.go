package main

import (
	"fmt"
	"strconv"

	"github.com/emirpasic/gods/stacks/arraystack"
)

const (
	Add          = 43
	Subtract     = 45
	Multiply     = 42
	Divide       = 47
	OpenBracket  = 40
	CloseBracket = 41
)

func Calc(a int, b int, operation int) int {
	switch operation {
	case Add:
		return a + b
	case Subtract:
		return a - b
	case Multiply:
		return a * b
	}

	return a / b
}

func Execute(operatorsStack *arraystack.Stack, numbersStack *arraystack.Stack, inBrackets bool) {
	a, _ := numbersStack.Pop()
	b, _ := numbersStack.Pop()

	op, _ := operatorsStack.Pop()
	result := Calc(b.(int), a.(int), op.(int))
	numbersStack.Push(result)

	if prevOperation, ok := operatorsStack.Peek(); ok {
		if prevOperation != nil && prevOperation.(int) != OpenBracket {
			Execute(operatorsStack, numbersStack, inBrackets)
		}

		if prevOperation.(int) == OpenBracket && inBrackets {
			operatorsStack.Pop()
		}
	}
}

func main() {
	expression := "50*((2+6)/(3-1)-12*((1+2)*2))"
	operatorsStack := arraystack.New()
	numbersStack := arraystack.New()
	currentNumber := ""

	priority := map[int]int{
		Add:          1,
		Subtract:     1,
		Multiply:     2,
		Divide:       2,
		OpenBracket:  3,
		CloseBracket: 3,
	}

	for index, token := range expression {
		intToken := int(token)
		if val, ok := priority[intToken]; ok {
			if len(currentNumber) > 0 {
				parsed, _ := strconv.Atoi(currentNumber)
				numbersStack.Push(parsed)
				currentNumber = ""
			}

			if intToken == OpenBracket {
				operatorsStack.Push(intToken)
				continue
			} else if intToken == CloseBracket {
				Execute(operatorsStack, numbersStack, true)
				continue
			}

			if prevOperation, ok := operatorsStack.Peek(); ok {
				if prevOperation.(int) != OpenBracket {
					prevVal, _ := priority[prevOperation.(int)]
					if prevVal >= val { // execute
						Execute(operatorsStack, numbersStack, false)
					}
				}
			}

			operatorsStack.Push(int(token))
		} else {
			currentNumber = currentNumber + string(token)
			if index >= len(expression)-1 {
				parsed, _ := strconv.Atoi(currentNumber)
				numbersStack.Push(parsed)
				Execute(operatorsStack, numbersStack, false)
			}
		}
	}

	if !operatorsStack.Empty() {
		Execute(operatorsStack, numbersStack, false)
	}

	fmt.Println(numbersStack)
}
